from django.db import models
from django.utils import timezone


# Create your models here.

class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length = 30)
    tempat = models.CharField(max_length = 30)
    kategori = models.CharField(max_length = 30)
    date = models.DateTimeField()
