from django.urls import path

from .views import *

app_name ='profilenadia'
urlpatterns = [
    path('', index, name='index'),
    path('portofolio/', portofolio, name = 'portofolio'),
    path('register/', register, name ='register'),
    path('schedule', schedule, name='schedule'),
    path('schedule_pos/', schedule_pos, name ='schedule_pos'),
    path('schedule_get', schedule_get, name='schedule_get'),
    path('delete_all', delete_all, name = 'delete_all'),
]
