from django import forms
from .models import Schedule

class Agenda_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
        }
    nama_kegiatan = forms.CharField(label= 'Nama Kegiatan', required = True, max_length= 27,
                           empty_value= 'Anonymous',widget= forms.TextInput(attrs=attrs))
    date = forms.DateField(label = 'Date', widget = forms.DateInput(attrs = {'type':'date'}))
    tempat = forms.CharField(label = 'Tempat', required = True, max_length= 27,
                            widget = forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label= 'Kategori', required = True, max_length= 27,
                           empty_value= 'Anonymous',widget= forms.TextInput(attrs=attrs))
    time = forms.TimeField(
        label = 'Time',
        widget = forms.TimeInput(attrs = {'type':'time'})
    )
