from django.shortcuts import render

# Create your views here.
from django.http import HttpResponseRedirect
from .forms import Agenda_Form
from .models import *
from . import forms


def index(request):
    response = {}
    return render(request, 'index.html', response)

def portofolio(request):
    response = {}
    return render(request, 'page2.html', response)

def register(request):
    response = {}
    return render(request, 'register.html', response)

def schedule_pos(request):
    form = Agenda_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {}
        response['nama_kegiatan'] = request.POST['nama_kegiatan']
        response['tempat'] = request.POST['tempat']
        response['kategori'] = request.POST['kategori']
        response['date'] = request.POST['date']
        schedule = Schedule(nama_kegiatan=response['nama_kegiatan'], tempat=response['tempat'], kategori=response['kategori'], date= response['date'])
        schedule.save()
        response = {}
        schedule = Schedule.objects.all()
        response['schedule'] = schedule
        html ='schedule.html'
        return render(request, html , response)
    else:
        return HttpResponseRedirect('/')

def schedule(request):
    response = {}
    response['Agenda_Form'] = Agenda_Form
    return render(request, 'create_sced.html', response)

def schedule_get(request):
    response = {}
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    html ='schedule.html'
    return render(request, html, response)


def delete_all(request):
    Schedule.objects.all().delete()
    response = {}
    html = 'schedule.html'
    return render(request, html, response)
